<?php $this->load->view('partials/header_view'); ?>

<body>
    <?php 
    //  if ($this->session->flashdata('msg')) {
    //   echo "<h3>" . $this->session->flashdata('msg') . "</h3>";
    // }
    ?>
    <?php if ($this->session->flashdata('errmsg')) {
        echo "<h3>" . $this->session->flashdata('errmsg') . "</h3>";
    }
    ?>
    <!-- BEGAIN PRELOADER -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- END PRELOADER -->

    <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="<?php echo base_url(); ?>#"><i class="fa fa-angle-up"></i></a>
    <!-- END SCROLL TOP BUTTON -->

    <!-- Start header -->

    <!-- End header -->

    <!-- Start login modal window -->
    <div aria-hidden="false" role="dialog" tabindex="-1" id="login-form" class="modal leread-modal fade in">
        <div class="modal-dialog">
            <!-- Start login section -->
            <div id="login-content" class="modal-content">
                <div class="modal-header">
                    <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span
                            aria-hidden="true">×</span></button>
                    <h4 class="modal-title"><i class="fa fa-unlock-alt"></i>Login</h4>
                </div>
                <div class="modal-body">

                    <?php echo validation_errors(); ?>
                    <?php echo form_open('User/Login'); ?>
                    <div class="form-group">
                        <input type="text" placeholder="User Name" class="form-control" name="username" required>
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" class="form-control" name="password" required>
                    </div>
                    <div class="loginbox">

                        <button class="btn signin-btn" type="submit">SIGN IN</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>

            </div>
            <!-- Start signup section -->

        </div>
    </div>
    <!-- End login modal window -->

    <!-- BEGIN MENU -->
    <section id="menu-area">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                        <!-- <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span> -->
                    </button>
                    <!-- LOGO -->
                    <!-- TEXT BASED LOGO -->

                    <!-- IMG BASED LOGO  -->
                    <!-- <a class="navbar-brand" href="index.html"><img src="assets/images/logo.png" alt="logo"></a> -->
                </div>

            </div>
        </nav>
    </section>
    <!-- END MENU -->

    <!-- Start slider -->
    <section id="slider">
        <div class="main-slider">
            <div class="single-slide">
                <img src="<?php echo base_url(); ?>assets/images/Poster.jpg" alt="img">
                <div class="slide-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="slide-article">
                                    <h1 class="wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">Welcome to
                                        the Metrial Management System</h1>
                                    <p class="wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.75s">Web portal
                                        of NEM Construction (pvt) Ltd
                                        <br>
                                        <br>
                                        <a style="color:#ff9900; font-size:26px" class="login modal-form"
                                            data-target="#login-form" data-toggle="modal"
                                            href="<?php echo base_url(); ?>#">
                                            ENTRY


                                        </a>

                                    </p>
                                </div>

                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="slider-img wow fadeInUp">
                                    <!-- <img src="<?php echo base_url(); ?>assets/images/person1.png" alt="business man"> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </section>
    <!-- End slider -->

    <!-- Start Feature -->


    <!-- Start about  -->


    <!-- Start footer -->

    <!-- End footer -->

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
    <!-- Slick Slider -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/slick.js"></script>
    <!-- mixit slider -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.mixitup.js"></script>
    <!-- Add fancyBox -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox.pack.js"></script>
    <!-- counter -->
    <script src="<?php echo base_url(); ?>assets/js/waypoints.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.counterup.js"></script>
    <!-- Wow animation -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/wow.js"></script>
    <!-- progress bar   -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-progressbar.js"></script>


    <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js" integrity="sha384-vhJnz1OVIdLktyixHY4Uk3OHEwdQqPppqYR8+5mjsauETgLOcEynD9oPHhhz18Nw" crossorigin="anonymous"></script> -->
</body>

</html>