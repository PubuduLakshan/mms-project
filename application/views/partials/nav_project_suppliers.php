
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"  type="text/css"rel="stylesheet" />

<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/jqueryajaxmin.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/new/jquerymin.js"></script>
    <script src="<?php echo base_url(); ?>assets/new/js/validate.js"></script>


<div class="data-table-area mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <h1>Supplier <span class="table-project-n">Details</span> </h1>
                        </div>
                    </div>
                    <div class="sparkline13-graph">
                        <div class="datatable-dashv1-list custom-datatable-overright">


                            <!-- Modal Form -->
                            <div class="sparkline11-graph">
                                <div class="basic-login-form-ad">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        
                                            <?php if ($this->session->userdata('VehicleNew') == "checked") { ?>
                                            <a class="zoomInDown mg-t" href="#" data-toggle="modal"
                                                data-target="#zoomInDown1">
                                                <button type="button" class="btn btn-primary">
                                                    <i class='far fa-file-alt'></i> New Supplier</button></a>
                                            <?php 
                                        } ?>

                                            <?php if ($this->session->userdata('VehicleView') == "checked") { ?>
                                                <a class="zoomInDown mg-t" href="#" data-toggle="modal"
                                                data-target="#zoomInDown2">
                                            <button type="button" class="btn btn-primary"><i class='fas fa-search'></i>
                                                View</button></a>
                                            <?php 
                                        } ?>

                                            <?php if ($this->session->userdata('VehicleEdit') == "checked") { ?>
                                                <a class="zoomInDown mg-t" href="#" data-toggle="modal"
                                                data-target="#zoomInDown3">
                                            <button type="button" class="btn btn-primary">
                                                <i class='fas fa-edit'></i> Edit</button></a>
                                            <?php 
                                        } ?>
                                            <?php if ($this->session->userdata('VehicleDelete') == "checked") { ?>
                                                <a class="zoomInDown mg-t" href="#" data-toggle="modal"
                                                data-target="#zoomInDown4">
                                            <button type="button" class="btn btn-primary"><i
                                                    class="far fa-trash-alt"></i>
                                                Delete</button></a>
                                            <?php 
                                        } ?>
                                            <div id="zoomInDown1" class="modal modal-edu-general modal-zoomInDown fade"
                                                role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header header-color-modal bg-color-1">
                                                            <h4 class="modal-title">Add New Supplier</h4>
                                                            <div class="modal-close-area modal-close-df">
                                                                <a class="close" data-dismiss="modal" href="#"><i
                                                                        class="fa fa-close"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="modal-login-form-inner">

                                                                <div class="row">
                                                                    <div
                                                                        class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div
                                                                            class="basic-login-inner modal-basic-inner">
                                                                            
                                                                            <form method="post" action="../Suppliers/register" class="add-department2">
                                                                            
                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                            <label
                                                                                                class="login2">Supplier Name</label>
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                            <input type="text"
                                                                                                class="form-control"
                                                                                                placeholder="Enter Supplier Name" name="suppliername" id="suppliername"/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                            <label class="login2">Supplier Code</label>
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                            <input type="text"
                                                                                                class="form-control"
                                                                                                placeholder="Enter Supplier Code" name="suppliercode" id="suppliercode" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group-inner">
                                                                                <div class="row">
                                                                                    <div
                                                                                        class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                        <label
                                                                                            class="login2">Contact Number</label>
                                                                                    </div>
                                                                                    <div
                                                                                            class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                            <input type="text"
                                                                                                class="form-control"
                                                                                                placeholder="Enter Contact Number" name="contactno" id="contactno"  />
                                                                                        </div>
                                                                                </div>
                                                                                </div>
                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                            <label
                                                                                                class="login2">Address</label>
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                            <input type="text"
                                                                                                class="form-control"
                                                                                                placeholder="Enter Address" name="address" name="address"/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                
                                                                                <div class="login-btn-inner">

                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                            <div
                                                                                                class="login-horizental">
                                                                                                <button
                                                                                                    class="btn btn-sm btn-primary login-submit-cs"
                                                                                                    type="submit">Add
                                                                                                    Supplier</button>

                                                                                                <a data-dismiss="modal"
                                                                                                    href="#"><button
                                                                                                        class="btn btn-sm btn-primary login-submit-cs"
                                                                                                        type="button">Cancel</button></a>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--Add Model END-->

                                             <!--View Model Start-->
                                        <div id="zoomInDown2" class="modal modal-edu-general modal-zoomInDown fade"
                                                role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header header-color-modal bg-color-1">
                                                            <h4 class="modal-title"> Supplier</h4>
                                                            <div class="modal-close-area modal-close-df">
                                                                <a class="close" data-dismiss="modal" href="#"><i
                                                                        class="fa fa-close"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="modal-login-form-inner">

                                                                <div class="row">
                                                                    <div
                                                                        class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="basic-login-inner modal-basic-inner">

<!-- Single pro tab review Start-->
        <div class="single-pro-review-area mt-t-30 mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <ul id="myTabedu1" class="tab-review-design">
                                <li class="active"><a href="#View"><i class="fas fa-search"></i> View</a></li>
                                <li><a href="#Edit"><i class="fas fa-edit"></i> Edit</a></li>
                                <li><a href="#Delete"><i class="fas fa-trash-alt"></i> Delete</a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content custom-product-edit">

                                <?php $this->load->view('partials/CRUD/Supplier/V_View.php'); ?>
                                <?php $this->load->view('partials/CRUD/Supplier/V_Edit.php'); ?>
                                <?php $this->load->view('partials/CRUD/Supplier/V_Delete.php'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
<!--END Single pro tab review Start-->                               
                                                                                
                                                                                
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--END View Model-->


                                            <!--Edit Model Start-->
                                        <div id="zoomInDown3" class="modal modal-edu-general modal-zoomInDown fade"
                                                role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header header-color-modal bg-color-1">
                                                            <h4 class="modal-title"> Supplier</h4>
                                                            <div class="modal-close-area modal-close-df">
                                                                <a class="close" data-dismiss="modal" href="#"><i
                                                                        class="fa fa-close"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="modal-login-form-inner">

                                                                <div class="row">
                                                                    <div
                                                                        class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="basic-login-inner modal-basic-inner">

<!-- Single pro tab review Start-->
        <div class="single-pro-review-area mt-t-30 mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <ul id="myTabedu1" class="tab-review-design">
                                <li><a href="#View1"><i class="fas fa-search"></i> View</a></li>
                                <li class="active"><a href="#Edit1"><i class="fas fa-edit"></i> Edit</a></li>
                                <li><a href="#Delete1"><i class="fas fa-trash-alt"></i> Delete</a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content custom-product-edit">

                                <?php $this->load->view('partials/CRUD/Supplier/E_View.php'); ?>
                                <?php $this->load->view('partials/CRUD/Supplier/E_Edit.php'); ?>
                                <?php $this->load->view('partials/CRUD/Supplier/E_Delete.php'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
<!--END Single pro tab review Start-->                               
                                                                                
                                                                                
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--END Edit Model-->


                                            <!--Edit Model Start-->
                                        <div id="zoomInDown4" class="modal modal-edu-general modal-zoomInDown fade"
                                                role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header header-color-modal bg-color-1">
                                                            <h4 class="modal-title"> Supplier</h4>
                                                            <div class="modal-close-area modal-close-df">
                                                                <a class="close" data-dismiss="modal" href="#"><i
                                                                        class="fa fa-close"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="modal-login-form-inner">

                                                                <div class="row">
                                                                    <div
                                                                        class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="basic-login-inner modal-basic-inner">

<!-- Single pro tab review Start-->
        <div class="single-pro-review-area mt-t-30 mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <ul id="myTabedu1" class="tab-review-design">
                                <li><a href="#View2"><i class="fas fa-search"></i> View</a></li>
                                <li><a href="#Edit2"><i class="fas fa-edit"></i> Edit</a></li>
                                <li class="active"><a href="#Delete2"><i class="fas fa-trash-alt"></i> Delete</a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content custom-product-edit">

                                <?php $this->load->view('partials/CRUD/Supplier/D_View.php'); ?>
                                <?php $this->load->view('partials/CRUD/Supplier/D_Edit.php'); ?>
                                <?php $this->load->view('partials/CRUD/Supplier/D_Delete.php'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
<!--END Single pro tab review Start-->                               
                                                                                
                                                                                
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--END View Model-->


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End modal -->
                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#addMyModal">Open Modal</button>
<div class="modal fade" id="addMyModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class='error_msg'></div>
        <h4 class="modal-title">Add Stuff</h4>
      </div>
      <div class="modal-body">
        <form role="form" id="newModalForm" class="add-department1">
          <div class="form-group">
            <label class="control-label col-md-3" for="email">A p Name:</label>
            <div class="col-md-9">
              <input type="text" class="form-control" id="pName" name="pName" placeholder="Enter a p name" require/>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3" for="email">Action:</label>
            <div class="col-md-9">
              <input type="text" class="form-control" id="action" name="action" placeholder="Enter and action" require>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success" id="sendform1">Save</button>
            <button type="button" class="btn btn-default" id="btnCloseIt" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>



                            <div class="row">

                                <table id="table" data-toggle="table" data-pagination="true" data-search="true"
                                    data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true"
                                    data-key-events="true" data-show-toggle="true" data-resizable="true"
                                    data-cookie="true" data-cookie-id-table="saveId" data-show-export="true"
                                    data-click-to-select="true" data-toolbar="#toolbar">
                                    <thead>

                                        <tr>
                                            
                                            <th data-field="vehicle">Supplier Name</th>
                                            <th data-field="vdescription">Supplier Code</th>
                                            <th data-field="type">Contact Number</th>
                                            <th data-field="vcapacity">Address</th>
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                            if ($suppliers_fetch_data->num_rows() > 0) {
                                                foreach ($suppliers_fetch_data->result() as $row) {
                                                    ?>

                                            <tr>
                                                
                                                <td>
                                                    <?php echo $row->SupplierName  ?>
                                                </td>
                                                <td>
                                                    <?php echo $row->SupplierCode  ?>
                                                </td>
                                                <td>
                                                    <?php echo $row->ContactNo  ?>
                                                </td>
                                                <td>
                                                    <?php echo $row->Address  ?>
                                                </td>
                                                
                                                
                                            </tr>


                                            <?php

                                        }
                                    } else {
                                        ?>
                                            <tr>
                                                <td colspan="3"> No Data Found </td>
                                            </tr>
                                            <?php 
                                        }
                                        ?>

                                        </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
     

   <script>
       (function ($) {
 "use strict";
   $(".add-department1").validate(
			{					
				rules: {
					pName: {
					  required: true,
					  minlength: 8
					},
					action: "required"
				  },
				  messages: {
					pName: {
					  required: "Please enter some data",
					  minlength: "Your data must be at least 8 characters"
					},
					action: "Please provide some data"
				  },			
				
				errorPlacement: function(error, element)
				{
					error.insertAfter(element.parent());
				}
            });
        })(jQuery); 
   </script> 




