<div class="mailbox-compose-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                   
                    <div class="col-md-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="hpanel email-compose">
                            <div class="panel-heading hbuilt">
                                <div class="p-xs h4">
                                    Compose E-mail 
                                </div>
                            </div>
                            <div class="panel-heading hbuilt">
                                <div class="p-xs">
                                    <form class="form-horizontal" method="post" action="../Email/send" enctype="multipart/form-data">
                                     <?php echo $this->session->flashdata('email_sent'); ?>
                                        <div class="form-group">
                                            <label class="col-lg-1 control-label text-left">To:</label>
                                            <div class="col-lg-11 col-md-12 col-sm-12 col-xs-12">
                                                <input name="email" id="email" type="text" class="form-control input-sm" placeholder="example@email.com">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-1 control-label text-left">Cc:</label>
                                            <div class="col-lg-11 col-md-12 col-sm-12 col-xs-12">
                                                <input id="recipient_email" name="recipient_email" type="text" class="form-control input-sm">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-1 control-label text-left">Subject:</label>
                                            <div class="col-lg-11 col-md-12 col-sm-12 col-xs-12">
                                                <input type="text" name="subject" id="subject" class="form-control input-sm"  placeholder="Subject">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-1 control-label text-left">Content:</label>
                                            <div class="col-lg-11 col-md-12 col-sm-12 col-xs-12">
                                            <textarea name="mailContent" id="mailContent" type="text" class="form-control" rows="5" placeholder="Content"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-1 control-label text-left">Files:</label>
                                            <div class="col-lg-11 col-md-12 col-sm-12 col-xs-12">
                                                <input type="file" name="file[]" multiple="multiple" id="file">
                                                <input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
                                            </div>
                                        </div>

                                        

                                <div class="pull-right">
                                    <div class="btn-group active-hook">
                                        <!-- <button class="btn btn-default"><i class="fa fa-edit"></i> Save</button>  -->
                                        <button class="btn btn-primary ft-compse"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send email</button>
                                </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>