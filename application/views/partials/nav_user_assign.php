<div class="data-table-area mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <h1>Users <span class="table-project-n">Assign</span> </h1>
                        </div>

                        <?php echo form_open('Assign/userAdd'); ?>
                        <div class="form-group-inner">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label class="login2">Name</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <select class="form-control" id="name1" name="name">
                                        <?php 
                                        if ($user_fetch_data->num_rows() > 0) {
                                            foreach ($user_fetch_data->result() as $row) {
                                                ?>

                                        <option><?php echo $row->Name  ?></option>
                                        <?php

                                    }
                                } else {
                                    ?>
                                        <option></option>
                                        <?php 
                                    }
                                    ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group-inner">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label class="login2">User Name</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">                               
                                <select class="form-control" id="uname" name="username">
                                <option>User Name</option>
                                </select>
                                </div>
                            </div>
                        </div>

                        <div class="login-btn-inner">

                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <div class="login-horizental">
                                        <button class="btn btn-sm btn-primary login-submit-cs" type="submit">Add
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>

                        <div class="row">


                            <table id="table" data-toggle="table" data-pagination="true" data-search="true"
                                data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true"
                                data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true"
                                data-toolbar="#toolbar">
                                <thead>
                                    <tr>
                                        <th> Name </th>
                                        <th> User Name </th>
                                        <?php 
                                        if ($project_assign_data->num_rows() > 0) {
                                            foreach ($project_assign_data->result() as $row) {
                                                ?>

                                        <th> <?php echo $row->ProjectName ?> </th>

                                        <?php

                                    }
                                }
                                ?>
                                    </tr>

                                </thead>
                                <tbody>
                                    <?php 
                                    if ($user_assign_data->num_rows() > 0) {
                                        foreach ($user_assign_data->result() as $row1) {
                                            ?>

                                    <tr>
                                        <td><?php echo $row1->Name  ?></td>
                                        <td><?php echo $row1->UserName  ?></td>

                                        <?php echo form_open('Assign/userAssign'); ?>
                                        <input type="hidden" name="UserName" value="<?php echo $row1->UserName  ?>" />
                                        <?php 
                                        if ($project_assign_data->num_rows() > 0) {
                                            foreach ($project_assign_data->result() as $row) {
                                                ?>
                                        <td><input type="checkbox" name="ProjectName[]"
                                                value="<?php echo $row->ProjectName ?>"
                                                <?php 
                                                                                                                                if ($userproject_fetch_data->num_rows() > 0) {
                                                                                                                                    foreach ($userproject_fetch_data->result() as $row2) {

                                                                                                                                        if ($row2->UserName == $row1->UserName && $row2->ProjectName == $row->ProjectName) {

                                                                                                                                            ?>
                                                <?php echo $row2->status  ?>
                                                <?php

                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                ?>><i></i>
                                        </td>


                                        <?php

                                    }
                                }
                                ?>
                                        <td><input type="submit" name="update" value="Assign"
                                                class="btn btn-sm btn-primary login-submit-cs" />
                                        </td>
                                        <?php echo form_close(); ?>
                                    </tr>

                                    <?php

                                }
                            }
                            ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>