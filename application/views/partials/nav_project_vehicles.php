<div class="data-table-area mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <h1>Vehicle <span class="table-project-n">Details</span> </h1>
                        </div>
                    </div>
                    <div class="sparkline13-graph">
                        <div class="datatable-dashv1-list custom-datatable-overright">


                            <!-- Modal Form -->
                            <div class="sparkline11-graph">
                                <div class="basic-login-form-ad">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        
                                            <?php if ($this->session->userdata('VehicleNew') == "YES") { ?>
                                            <a class="zoomInDown mg-t" href="#" data-toggle="modal"
                                                data-target="#zoomInDown1">
                                                <button type="button" class="btn btn-primary">
                                                    <i class='far fa-file-alt'></i> New Vehicle</button></a>
                                            <?php 
                                        } ?>
                                            <?php if ($this->session->userdata('VehicleView') == "YES") { ?>
                                            <button type="button" class="btn btn-primary"><i class='far fa-eye'></i>
                                                View</button>
                                            <?php 
                                        } ?>
                                            <?php if ($this->session->userdata('VehicleEdit') == "YES") { ?>

                                            <button type="button" class="btn btn-primary">
                                                <i class='fas fa-edit'></i> Edit</button>
                                            <?php 
                                        } ?>
                                            <?php if ($this->session->userdata('VehicleDelete') == "YES") { ?>

                                            <button type="button" class="btn btn-primary"><i
                                                    class="far fa-trash-alt"></i>
                                                Delete</button>
                                            <?php 
                                        } ?>
                                            <div id="zoomInDown1" class="modal modal-edu-general modal-zoomInDown fade"
                                                role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header header-color-modal bg-color-1">
                                                            <h4 class="modal-title">Add New Vehicle</h4>
                                                            <div class="modal-close-area modal-close-df">
                                                                <a class="close" data-dismiss="modal" href="#"><i
                                                                        class="fa fa-close"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="modal-login-form-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="basic-login-inner modal-basic-inner">

                                                                        <?php echo form_open('Vehicle/register'); ?>
                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                            <label
                                                                                                class="login2">Vehicle Number</label>
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                            <input type="text"
                                                                                            name="vehiclenumber" id="vehiclenumber" class="form-control"
                                                                                                placeholder="Enter Vehicle Number"  />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                            <label class="login2">Description</label>
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                            <input type="text"
                                                                                            class="form-control" id="description" name="description"
                                                                                                placeholder="Enter Description"  />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group-inner">
                                                                                <div class="row">
                                                                                    <div
                                                                                        class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                        <label
                                                                                            class="login2">Type</label>
                                                                                    </div>
                                                                                <div
                                                                                        class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                        <select class="form-control"
                                                                                            id="sel1" name="type">
                                                                                           
                                                                                            <option>Internal</option>
                                                                                            <option>External</option>
                                                                                            
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                </div>
                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                            <label
                                                                                                class="login2">Capacity</label>
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                            <input type="text"
                                                                                                class="form-control"
                                                                                                placeholder="Enter Capacity" name="capacity" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                            <label
                                                                                                class="login2">UOM</label>
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                            <input type="text"
                                                                                                class="form-control"
                                                                                                placeholder="Enter Units of Materials" name="uom" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="login-btn-inner">

                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                            <div
                                                                                                class="login-horizental">
                                                                                                <button
                                                                                                    class="btn btn-sm btn-primary login-submit-cs"
                                                                                                    type="submit">Add
                                                                                                    Vehicle</button>

                                                                                                <a data-dismiss="modal"
                                                                                                    href="#"><button
                                                                                                        class="btn btn-sm btn-primary login-submit-cs"
                                                                                                        type="button">Cancel</button></a>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <?php echo form_close(); ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End modal -->



                            <div class="row">

                                <table id="table" data-toggle="table" data-pagination="true" data-search="true"
                                    data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true"
                                    data-key-events="true" data-show-toggle="true" data-resizable="true"
                                    data-cookie="true" data-cookie-id-table="saveId" data-show-export="true"
                                    data-click-to-select="true" data-toolbar="#toolbar">
                                    <thead>

                                        <tr>
                                            <th data-field="state" data-radio="true"></th>
                                            <th data-field="vehicle">Vehicle No</th>
                                            <th data-field="vdescription">Description</th>
                                            <th data-field="type">Type</th>
                                            <th data-field="vcapacity">Capacity</th>
                                            <th data-field="uom">UOM</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                            if ($vehicle_fetch_data->num_rows() > 0) {
                                                foreach ($vehicle_fetch_data->result() as $row) {
                                                    ?>

                                            <tr>
                                                <td></td>
                                                <td>
                                                    <?php echo $row->VehicleNo  ?>
                                                </td>
                                                <td>
                                                    <?php echo $row->Description  ?>
                                                </td>
                                                <td>
                                                    <?php echo $row->VehicleType  ?>
                                                </td>
                                                <td>
                                                    <?php echo $row->Capacity  ?>
                                                </td>
                                                <td>
                                                    <?php echo $row->UOM  ?>
                                                </td>
                                                
                                            </tr>


                                            <?php

                                        }
                                    } else {
                                        ?>
                                            <tr>
                                                <td colspan="3"> No Data Found </td>
                                            </tr>
                                            <?php 
                                        }
                                        ?>
                                       
                                        </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>