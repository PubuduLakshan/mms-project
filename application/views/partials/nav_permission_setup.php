<div class="data-table-area mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <h1>Permission <span class="table-project-n">Setup</span> </h1>
                        </div>

                        <?php echo form_open('Permission/userAdd'); ?>
                        <div class="form-group-inner">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label class="login2">User Name</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <select class="form-control" id="uname1" name="UserName">
                                        <?php 
                                        if ($user_fetch_data->num_rows() > 0) {
                                                foreach ($user_fetch_data->result() as $row) {
                                                        ?>

                                        <option><?php echo $row->UserName  ?></option>
                                        <?php
                                }
                        } else {
                                ?>
                                        <option></option>
                                        <?php 
                                }
                                ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group-inner">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label class="login2">Postion</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <select class="form-control" id="position" name="Position">
                                    <option>Position</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="login-btn-inner">

                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    
                                        <button class="btn btn-sm btn-primary login-submit-cs" type="submit">Add
                                        </button>
 
                                </div>
                            </div>
                        </div><br>
                        <?php echo form_close(); ?>

                        <div class="row">


                            <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                <thead>

                                    <tr>

                                        <th rowspan=2 data-field="username">Username</th>
                                        <th rowspan=2 data-field="position">Position</th>
                                        <th colspan=4 data-field="projects">Projects</th>
                                        <th colspan=5 data-field="users">Users</th>
                                        <th colspan=5 data-field="qrcon">QR Code Configuration</th>
                                        <th rowspan=2 data-field="qrgen">QR Code Generation</th>
                                        <th colspan=5 data-field="supplires">Suppliers</th>
                                        <th colspan=4 data-field="locations">Locations</th>
                                        <th colspan=4 data-field="vehicles">Vehicles</th>
                                        <th colspan=4 data-field="supveri">Supplier Quantity Verification Data</th>
                                        <th colspan=2 data-field="supquantity">Supplier Quantity</th>
                                        <th rowspan=2 data-field="report">Reports Generation</th>
                                        <th rowspan=2 data-field="permissoin">Permission Setup </th>
                                        <th rowspan=2 data-field="projassign">Project Assign</th>
                                        <th rowspan=2 data-field="proj">AAAA</th>
                                        
                                    </tr>
                                    <tr>

                                        <th data-field="prnew">New</th>
                                        <th data-field="prview">View</th>
                                        <th data-field="predit">Edit</th>
                                        <th data-field="prdelete">Delete</th>

                                        <th data-field="unew">New</th>
                                        <th data-field="uview">View</th>
                                        <th data-field="uedit">Edit</th>
                                        <th data-field="udelete">Delete</th>
                                        <th data-field="ublock">Block</th>

                                        <th data-field="qrnew">New</th>
                                        <th data-field="qrview">View</th>
                                        <th data-field="qredit">Edit</th>
                                        <th data-field="qrdelete">Delete</th>
                                        <th data-field="qrblock">Block</th>

                                        <th data-field="supnew">New</th>
                                        <th data-field="supview">View</th>
                                        <th data-field="supedit">Edit</th>
                                        <th data-field="supdelete">Delete</th>
                                        <th data-field="supblock">Block</th>

                                        <th data-field="locnew">New</th>
                                        <th data-field="locview">View</th>
                                        <th data-field="locedit">Edit</th>
                                        <th data-field="locdelete">Delete</th>

                                        <th data-field="vnew">New</th>
                                        <th data-field="vview">View</th>
                                        <th data-field="vedit">Edit</th>
                                        <th data-field="vdelete">Delete</th>


                                        <th data-field="sqverview">View</th>
                                        <th data-field="sqveredit">Edit</th>
                                        <th data-field="sqverpost">Post</th>
                                        <th data-field="sqvermanual">Manual</th>

                                        <th data-field="sqiew">View</th>
                                        <th data-field="sqedit">Edit</th>

                                        
                                    </tr>

                                </thead>
                                <tbody>

                                    <?php 
                                        if ($user_permission_data->num_rows() > 0) {
                                                foreach ($user_permission_data->result() as $row) {
                                                        ?>

                                    <tr>

                                        <td><?php echo $row->UserName  ?></td>
                                        <td><?php echo $row->Position  ?></td>

                                        <?php echo form_open('Permission/grant'); ?>
                                        <input type="hidden" name="UserName" value="<?php echo $row->UserName  ?>">

                                        <td><input type="checkbox" name="ProjectNew" value="checked" <?php echo  $row->ProjectNew ?>><i></i></td>
                                        <td><input type="checkbox" name="ProjectView" value="checked" <?php echo  $row->ProjectView ?>><i></i> </td>
                                        <td><input type="checkbox" name="ProjectEdit" value="checked" <?php echo  $row->ProjectEdit ?>><i></i> </td>
                                        <td><input type="checkbox" name="ProjectDelete" value="checked" <?php echo  $row->ProjectDelete ?>><i></i> </td>

                                        <td><input type="checkbox" name="UserNew" value="checked" <?php echo  $row->UserNew ?>><i></i> </td>
                                        <td><input type="checkbox" name="UserView" value="checked" <?php echo  $row->UserView ?>><i></i> </td>
                                        <td><input type="checkbox" name="UserEdit" value="checked" <?php echo  $row->UserEdit ?>><i></i> </td>
                                        <td><input type="checkbox" name="UserDelete" value="checked" <?php echo  $row->UserDelete ?>><i></i> </td>
                                        <td><input type="checkbox" name="UserBlock" value="checked" <?php echo  $row->UserBlock ?>><i></i> </td>

                                        <td><input type="checkbox" name="QRCodeAdd" value="checked" <?php echo  $row->QRCodeAdd ?>><i></i> </td>
                                        <td><input type="checkbox" name="QRCodeView" value="checked" <?php echo  $row->QRCodeView ?>><i></i> </td>
                                        <td><input type="checkbox" name="QRCodeEdit" value="checked" <?php echo  $row->QRCodeEdit ?>><i></i> </td>
                                        <td><input type="checkbox" name="QRCodeDelete" value="checked" <?php echo  $row->QRCodeDelete ?>><i></i> </td>
                                        <td><input type="checkbox" name="QRCodeBlock" value="checked" <?php echo  $row->QRCodeBlock ?>><i></i> </td>

                                        <td><input type="checkbox" name="QRCodeGeneration" value="checked" <?php echo  $row->QRCodeGeneration ?>><i></i> </td>

                                        <td><input type="checkbox" name="SupplierNew" value="checked" <?php echo  $row->SupplierNew ?>><i></i> </td>
                                        <td><input type="checkbox" name="SupplierView" value="checked" <?php echo  $row->SupplierView ?>><i></i> </td>
                                        <td><input type="checkbox" name="SupplierEdit" value="checked" <?php echo  $row->SupplierEdit ?>><i></i> </td>
                                        <td><input type="checkbox" name="SupplierDelete" value="checked" <?php echo  $row->SupplierDelete ?>><i></i> </td>
                                        <td><input type="checkbox" name="SupplierBlock" value="checked" <?php echo  $row->SupplierBlock ?>><i></i> </td>

                                        <td><input type="checkbox" name="LocationNew" value="checked" <?php echo  $row->LocationNew ?>><i></i> </td>
                                        <td><input type="checkbox" name="LocationView" value="checked" <?php echo  $row->LocationView ?>><i></i> </td>
                                        <td><input type="checkbox" name="LocationEdit" value="checked" <?php echo  $row->LocationEdit ?>><i></i> </td>
                                        <td><input type="checkbox" name="LocationDelete" value="checked" <?php echo  $row->LocationDelete ?>><i></i> </td>

                                        <td><input type="checkbox" name="VehicleNew" value="checked" <?php echo  $row->VehicleNew ?>><i></i> </td>
                                        <td><input type="checkbox" name="VehicleView" value="checked" <?php echo  $row->VehicleView ?>><i></i> </td>
                                        <td><input type="checkbox" name="VehicleEdit" value="checked" <?php echo  $row->VehicleEdit ?>><i></i> </td>
                                        <td><input type="checkbox" name="VehicleDelete" value="checked" <?php echo  $row->VehicleDelete ?>><i></i> </td>

                                        <td><input type="checkbox" name="SQVDView" value="checked" <?php echo  $row->SQVDView ?>><i></i> </td>
                                        <td><input type="checkbox" name="SQVDEdit" value="checked" <?php echo  $row->SQVDEdit ?>><i></i> </td>
                                        <td><input type="checkbox" name="SQVDPost" value="checked" <?php echo  $row->SQVDPost ?>><i></i> </td>
                                        <td><input type="checkbox" name="SQVDManual" value="checked" <?php echo $row->SQVDManual ?>><i></i> </td>


                                        <td><input type="checkbox" name="SQView" value="checked" <?php echo  $row->SQView ?>><i></i> </td>
                                        <td><input type="checkbox" name="SQEdit" value="checked" <?php echo  $row->SQEdit ?>><i></i> </td>

                                        <td><input type="checkbox" name="ReportGen" value="checked" <?php echo  $row->ReportGen ?>><i></i> </td>

                                        <td><input type="checkbox" name="PermissionSetup" value="checked" <?php echo  $row->PermissionSetup ?>><i></i> </td>


                                        <td><input type="checkbox" name="ProjectAssign" value="checked" <?php echo  $row->ProjectAssign ?>><i></i> </td>

                                                    <td></td>
                                        <td  class="fixed-button"><input type="submit" name="update" value="Apply" class="btn btn-sm btn-primary login-submit-cs" />
                                        </td> <?php echo form_close(); ?>
                                    </tr>

                                    <?php

                                }
                        } else {
                                ?>
                                    <tr>
                                        <td colspan="3"> No Data Found </td>
                                    </tr>
                                    <?php 
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
