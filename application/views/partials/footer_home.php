<div class="footer-copyright-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="footer-copy-right">
                    <p>&nbsp;&nbsp; NEM Constructions (Pvt) Ltd &nbsp;&nbsp;&nbsp;&nbsp;<?php echo $this->session->userdata('name');?>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo date("m/d/y");?> 
                    </p>

                </div>
            </div>
        </div>
    </div>
</div>
</div>



<!-- jquery
		============================================ -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/vendor/jquery-1.12.4.min.js">
</script>
<!-- bootstrap JS
		============================================ -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/bootstrap.min.js"></script>
<!-- wow JS
		============================================ -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/wow.min.js"></script>
<!-- price-slider JS
		============================================ -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/jquery-price-slider.js"></script>
<!-- meanmenu JS
		============================================ -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/jquery.meanmenu.js"></script>
<!-- owl.carousel JS
		============================================ -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/owl.carousel.min.js"></script>
<!-- sticky JS
		============================================ -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/jquery.sticky.js"></script>
<!-- scrollUp JS
		============================================ -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/jquery.scrollUp.min.js"></script>
<!-- mCustomScrollbar JS
		============================================ -->
<script type="text/javascript"
    src="<?php echo base_url(); ?>assets/new/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/scrollbar/mCustomScrollbar-active.js">
</script>
<!-- metisMenu JS
		============================================ -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/metisMenu/metisMenu.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/metisMenu/metisMenu-active.js">
</script>

    
    <!-- summernote JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/new/js/summernote/summernote.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/new/js/summernote/summernote-active.js"></script>
    <!-- form validate JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/new/js/form-validation/jquery.form.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/new/js/form-validation/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/new/js/form-validation/form-active.js"></script>
    <!-- multiple email JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/new/js/multiple-email/multiple-email-active.js"></script>
    <!-- dropzone JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/new/js/dropzone/dropzone.js"></script>
    <!-- tab JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/new/js/tab.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/new/js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="<?php echo base_url(); ?>assets/new/js/main.js"></script>
    <!-- Chart JS
		============================================ -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/chart/jquery.peity.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/peity/peity-active.js"></script>
<!-- tab JS
		============================================ -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/tab.js"></script>


<!-- data table JS
		============================================ -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/data-table/bootstrap-table.js">
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/data-table/tableExport.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/data-table/data-table-active.js">
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/data-table/bootstrap-table-editable.js">
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/data-table/bootstrap-editable.js">
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/data-table/bootstrap-table-resizable.js">
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/data-table/colResizable-1.5.source.js">
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/data-table/bootstrap-table-export.js">
</script>
<!--  editable JS
		============================================ -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/editable/jquery.mockjax.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/editable/mock-active.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/editable/select2.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/editable/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/editable/bootstrap-datetimepicker.js">
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/editable/bootstrap-editable.js">
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/new/js/editable/xediable-active.js">
</script>


<!--  Excel JS
		============================================ -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.4/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>


 <!-- pwstrength JS
		============================================ -->
        <script src="<?php echo base_url(); ?>assets/new/js/password-meter/pwstrength-bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/new/js/password-meter/zxcvbn.js"></script>
    <script src="<?php echo base_url(); ?>assets/new/js/password-meter/password-meter-active.js"></script>

    <!-- form validate JS
		============================================ -->
        <script src="<?php echo base_url(); ?>assets/new/js/form-validation/jquery.form.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/new/js/form-validation/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/new/js/form-validation/form-active.js"></script>

    <script>
       (function ($) {
 "use strict";
   $(".add-department2").validate(
			{			  
		
			rules: {
                suppliername:
				{
                    required: true,
                    minlength: 8
				},
				suppliercode:
				{
					required: true
                },
                contactno:
				{
					required: true
                },
                address:
				{
                    required: true
                }
			},
				
			messages: {
                suppliername:
				{
					required: 'Please enter'
				},
				suppliercode:
				{
					required: '<b>Please</b> enter'
                },
                contactno:
				{
					required: 'Please enter'
                },
                address:
				{
                    required: 'Please enter'
                }
			},		
				
				errorPlacement: function(error, element)
				{
					error.insertAfter(element.parent());
				}
            });
        })(jQuery); 
   </script> 


<!-- Excel Sheet Generator -->
<script>
function exportToExcel(tableID){
    var tab_text="<table border='2px'><tr bgcolor='#87AFC6' style='height: 75px; text-align: center; width: 250px'>";
    var textRange; var j=0;
    tab = document.getElementById(tableID); // id of table
    for(j = 0 ; j < tab.rows.length ; j++)
    {
        tab_text=tab_text;
        tab_text=tab_text+tab.rows[j].innerHTML.toUpperCase()+"</tr>";
        //tab_text=tab_text+"</tr>";
    }
    tab_text= tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); //remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); //remove input params
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write( 'sep=,\r\n' + tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa=txtArea1.document.execCommand("SaveAs",true,"sudhir123.txt");
    }
    else {
       sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
    }
    
    return (sa);
}
</script>
<!-- End Excel Sheet Generator -->



<!-- Username validation -->
<script type="text/javascript">
 $(document).ready(function(){
  $('#username1').change(function(){
   var username1 = $('#username1').val();
   if(username1 != ''){
    $.ajax({
     url: "checkUsername",
     method: "POST",
     data: {username1:username1},
     success: function(data){
      $('#username_result').html(data);
     }
    });
   }
  });
 });
</script>

<!-- Password length validation -->
<script>
var minLength = 5;
$(document).ready(function(){
    $('#password1').on('keydown keyup change', function(){
        var char = $(this).val();
        var charLength = $(this).val().length;
        if(charLength < minLength){
            $('#submit').attr('disabled', true);
            $('#pwd').css('color', 'red');
            $('#pwd').text('x Length is short, minimum'+minLength+' required.');
        }
        else{
            $('#submit').removeAttr('disabled');
            $('#pwd').css('color', 'green');
            $('#pwd').text('Length is valid');
        }
    });
});
</Script>


<!-- Username length validation -->
<script>
var minLength = 5;
$(document).ready(function(){
    $('#username1').on('keydown keyup change', function(){
        var char = $(this).val();
        var charLength = $(this).val().length;
        if(charLength < minLength){
            $('#submit').attr('disabled', true);
            $('#uname').css('color', 'red');
            $('#uname').text('x Length is short, minimum '+minLength+' required.');
        }else{
            $('#submit').removeAttr('disabled');
            $('#uname').css('color', 'green');
            $('#uname').text('Length is valid');
        }
    });
});
</Script>

<!-- Contact number length validation -->

<script>
var Length = 10;
$(document).ready(function(){
    $('#contact1').on('keydown keyup change', function(){
var mobileNum = $(this).val();
var validateMobNum= /^\d*(?:\.\d{1,2})?$/;
if (validateMobNum.test(mobileNum ) && mobileNum.length == Length) {
    
    $('#submit').removeAttr('disabled');
    $('#cont').css('color', 'green');
    $('#cont').text('Valid phone number');
}
else {
    $('#submit').attr('disabled', true);
    $('#cont').css('color', 'red');
    $('#cont').text('x Invalid number type/length');
}

});
});
</script>

<!-- Submit button disable function -->
<script>
   
var $input = $('input:text'),
    $register = $('#submit');

   
$register.attr('disabled', true);
$input.keyup(function() {
    var trigger = false;
    $input.each(function() {
        if (!$(this).val()) {
            trigger = true;
        }
    });
    trigger ? $register.attr('disabled', true) : $register.removeAttr('disabled');
});

</script>
<!-- End Submit button disable function-->
<script>

</script>

<!-- Auto filling permission -->
<script>
$(document).ready(function(){
 $('#uname1').change(function(){
  var uname1 = $('#uname1').val();
  if(uname1 != '')
  {
   $.ajax({
    url:"autofill",
    method:"POST",
    data:{uname1:uname1},
    success:function(data)
    {
     $('#position').html(data);
    
    }
   })
}
 })
  });
</script>
<!--END Auto filling permission -->

<!-- Auto filling assign -->
<script>
$(document).ready(function(){
 $('#name1').change(function(){
  var name1 = $('#name1').val();
  if(name1 != '')
  {
   $.ajax({
    url:"autofillAssign",
    method:"POST",
    data:{name1:name1},
    success:function(data)
    {
     $('#uname').html(data);
    
    }
   })
}
 })
  });
</script>
<!--END Auto filling assign -->

</body>

</html>