<div class="data-table-area mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <h1>User <span class="table-project-n">Details</span> </h1>
                        </div>
                    </div>
                    <div class="sparkline13-graph">
                        <div class="datatable-dashv1-list custom-datatable-overright">

                            <!-- Modal Form -->
                            <div class="sparkline11-graph">
                                <div class="basic-login-form-ad">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        
                                        <?php if ($this->session->userdata('UserNew') == "checked") { ?>
                                            <a class="zoomInDown mg-t" href="#" data-toggle="modal"
                                                data-target="#zoomInDown1">
                                                <button type="button" class="btn btn-primary">
                                                    <i class='fas fa-user-plus'></i> New User</button></a>
                                            <?php 
                                            } ?>
                                            <?php if ($this->session->userdata('UserView') == "checked") { ?>
                                            <a class="zoomInDown mg-t" href="#" data-toggle="modal"
                                                data-target="#zoomInDown2">
                                                <button type="button" class="btn btn-primary"><i class='fas fa-search'></i>
                                                    View</button></a>
                                            <?php 
                                        } ?>
                                            <?php if ($this->session->userdata('UserEdit') == "checked") { ?>
                                                <a class="zoomInDown mg-t" href="#" data-toggle="modal"
                                                data-target="#zoomInDown3">
                                            <button type="button" class="btn btn-primary">
                                                <i class='fas fa-user-edit'></i> Edit</button></a>
                                            <?php 
                                        } ?>
                                            <?php if ($this->session->userdata('UserDelete') == "checked") { ?>
                                                <a class="zoomInDown mg-t" href="#" data-toggle="modal"
                                                data-target="#zoomInDown4">
                                            <button type="button" class="btn btn-primary"><i class="far fa-trash-alt"
                                                    name="delete_all" id="delte_all"></i>
                                                Delete</button></a>
                                            <?php 
                                        } ?>

                                        <a href="../Permission/permissionView">
                                        <button type="submit" class="btn btn-primary">
                                        <i class="fas fa-user-lock" aria-hidden="true"></i> Permission Setup</button></a>

                                        <a href="../Assign/assignView">
                                        <button type="submit" class="btn btn-primary">
                                        <i class="fas fa-paste"></i> Project Assigning</button></a>
                                                
                                        <button onclick="exportToExcel('table')" type="button" class="btn btn-primary">
                                        <i class="far fa-file-excel"></i> Export Excel</button></a>
                                               
                                        <a href="../User/users">
                                        <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-refresh" aria-hidden="true"></i> Refresh</button></a>
                                           
                                           <div id="zoomInDown1" class="modal modal-edu-general modal-zoomInDown fade"
                                                role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header header-color-modal bg-color-1">
                                                            <h4 class="modal-title">Add New User</h4>
                                                            <div class="modal-close-area modal-close-df">
                                                                <a class="close" data-dismiss="modal" href="#"><i
                                                                        class="fa fa-close"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="modal-login-form-inner">

                                                                <div class="row">
                                                                    <div
                                                                        class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div
                                                                            class="basic-login-inner modal-basic-inner">
                                                                            
                                                                            <?php echo form_open('User/register'); ?>
                                                                            <form action="#" method="post" onsubmit="return ValidationEvent()">
                                                                            <div class="form-group-inner">
                                                                                <div class="row">
                                                                                    <div
                                                                                        class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                        <label class="login2">
                                                                                            Name</label>
                                                                                    </div>
                                                                                    <div
                                                                                        class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                        <input type="text" name="name" id="name"
                                                                                            class="form-control"
                                                                                            placeholder="Full Name"/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group-inner">
                                                                                <div class="row">
                                                                                    <div
                                                                                        class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                        <label class="login2">User
                                                                                            Name
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                        
                                                                                        <input type="text"
                                                                                            class="form-control"
                                                                                            name="username" id="username1"
                                                                                            placeholder="User Name"
                                                                                             />
                                                                                            <div  role="alert" id="uname"></div>
                                                                                            <div  role="alert" id="username_result"></div>                                                                                            
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                         
                                                                            
                                                                            <div class="form-group-inner">
                                                                                <div class="row">
                                                                                    <div
                                                                                        class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                        <label class="login2">Password
                                                                                        </label>
                                                                                    </div>
                                                                                    <div
                                                                                        class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                        <input type="password"
                                                                                            class="form-control example1"
                                                                                            name="password" id="password1"
                                                                                            placeholder="Password"
                                                                                             />
                                                                                             <div class="form-group mg-b-pass"><div class="pwstrength_viewport_progress"></div></div>
                                                                                            <div role="alert" id="pwd"></div>
                                                                                            <div role="alert" id="password_result">
                                                                                            </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group-inner">
                                                                                <div class="row">
                                                                                    <div
                                                                                        class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                        <label
                                                                                            class="login2">Catagory</label>
                                                                                    </div>

                                                                                    <div
                                                                                        class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                        <select class="form-control"
                                                                                            id="sel1" name="catagory">
                                                                                            <option>System Admin
                                                                                            </option>
                                                                                            <option>Head Office Admin
                                                                                            </option>
                                                                                            <option>Project Manager
                                                                                            </option>
                                                                                            <option>Site QS</option>
                                                                                            <option>Site SK</option>
                                                                                            <option>Site ASK</option>
                                                                                        </select>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group-inner">
                                                                                <div class="row">
                                                                                    <div
                                                                                        class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                        <label class="login2">Contact
                                                                                            Number</label>
                                                                                    </div>
                                                                                    <div
                                                                                        class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                        <input type="text"
                                                                                            class="form-control"
                                                                                            placeholder="Contact Number"
                                                                                            name="contact" id="contact1"
                                                                                             />

                                                                                            <div role="alert" id="cont"></div>
                                                                                            
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="login-btn-inner">

                                                                                <div class="row">
                                                                                    <div
                                                                                        class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                    </div>
                                                                                    <div
                                                                                        class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                        <div class="login-horizental">
                                                                                            <button
                                                                                                class="btn btn-sm btn-primary login-submit-cs"
                                                                                                type="submit"
                                                                                                name="submit" id="submit"
                                                                                                >Add
                                                                                                User</button>
                                                                                            <a data-dismiss="modal"
                                                                                                href="#"><button
                                                                                                    class="btn btn-sm btn-primary login-submit-cs"
                                                                                                    type="button"  >Cancel</button></a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                                </form>
                                                                            <?php echo form_close(); ?>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        <!--Add Model END-->


                                        <!--View Model Start-->
                                        <div id="zoomInDown2" class="modal modal-edu-general modal-zoomInDown fade"
                                                role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header header-color-modal bg-color-1">
                                                            <h4 class="modal-title">User</h4>
                                                            <div class="modal-close-area modal-close-df">
                                                                <a class="close" data-dismiss="modal" href="#"><i
                                                                        class="fa fa-close"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="modal-login-form-inner">

                                                                <div class="row">
                                                                    <div
                                                                        class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="basic-login-inner modal-basic-inner">

<!-- Single pro tab review Start-->
        <div class="single-pro-review-area mt-t-30 mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <ul id="myTabedu1" class="tab-review-design">
                                <li class="active"><a href="#View"><i class="fas fa-search"></i> View</a></li>
                                <li><a href="#Edit"><i class="fas fa-edit"></i> Edit</a></li>
                                <li><a href="#Delete"><i class="fas fa-trash-alt"></i> Delete</a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content custom-product-edit">

                                <?php $this->load->view('partials/CRUD/User/V_View.php'); ?>
                                <?php $this->load->view('partials/CRUD/User/V_Edit.php'); ?>
                                <?php $this->load->view('partials/CRUD/User/V_Delete.php'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
<!--END Single pro tab review Start-->                               
                                                                                
                                                                                
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--END View Model-->


                                             <!--Edit Model Start-->
                                        <div id="zoomInDown3" class="modal modal-edu-general modal-zoomInDown fade"
                                                role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header header-color-modal bg-color-1">
                                                            <h4 class="modal-title">New Supplier</h4>
                                                            <div class="modal-close-area modal-close-df">
                                                                <a class="close" data-dismiss="modal" href="#"><i
                                                                        class="fa fa-close"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="modal-login-form-inner">

                                                                <div class="row">
                                                                    <div
                                                                        class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="basic-login-inner modal-basic-inner">

<!-- Single pro tab review Start-->
        <div class="single-pro-review-area mt-t-30 mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <ul id="myTabedu1" class="tab-review-design">
                                <li><a href="#View1"><i class="fas fa-search"></i> View</a></li>
                                <li class="active"><a href="#Edit1"><i class="fas fa-edit"></i> Edit</a></li>
                                <li><a href="#Delete1"><i class="fas fa-trash-alt"></i> Delete</a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content custom-product-edit">

                                <?php $this->load->view('partials/CRUD/User/E_View.php'); ?>
                                <?php $this->load->view('partials/CRUD/User/E_Edit.php'); ?>
                                <?php $this->load->view('partials/CRUD/User/E_Delete.php'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
<!--END Single pro tab review Start-->                               
                                                                                
                                                                                
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--END Edit Model-->


                                   <!--Delete Model Start-->
                                   <div id="zoomInDown4" class="modal modal-edu-general modal-zoomInDown fade"
                                                role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header header-color-modal bg-color-1">
                                                            <h4 class="modal-title">New Supplier</h4>
                                                            <div class="modal-close-area modal-close-df">
                                                                <a class="close" data-dismiss="modal" href="#"><i
                                                                        class="fa fa-close"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="modal-login-form-inner">

                                                                <div class="row">
                                                                    <div
                                                                        class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="basic-login-inner modal-basic-inner">

<!-- Single pro tab review Start-->
        <div class="single-pro-review-area mt-t-30 mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <ul id="myTabedu1" class="tab-review-design">
                                <li><a href="#View2"><i class="fas fa-search"></i> View</a></li>
                                <li><a href="#Edit2"><i class="fas fa-edit"></i> Edit</a></li>
                                <li class="active"><a href="#Delete2"><i class="fas fa-trash-alt"></i> Delete</a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content custom-product-edit">

                                <?php $this->load->view('partials/CRUD/User/D_View.php'); ?>
                                <?php $this->load->view('partials/CRUD/User/D_Edit.php'); ?>
                                <?php $this->load->view('partials/CRUD/User/D_Delete.php'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
<!--END Single pro tab review Start-->                               
                                                                                
                                                                                
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--END Delete Model-->
          

                                            

                                        
                                       
                                        
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                            <!--END Modal Form -->


                                           

                            <div class="row">

                                <form name="bulk_action_form" action="" method="post"
                                    onSubmit="return delete_confirm();">
                                    <table id="table" data-toggle="table" data-pagination="true" data-search="true"
                                        data-show-columns="true" data-show-pagination-switch="true"
                                        data-show-refresh="true" data-key-events="true" data-show-toggle="true"
                                        data-resizable="true" data-cookie="true" data-cookie-id-table="saveId"
                                        data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                        <thead>

                                            <tr>
                                                
                                                <th data-field="id">Name</th>
                                                <th data-field="name">Username</th>
                                                <th data-field="catergory">Catergory</th>
                                                <th data-field="phone">Contact No</th>

                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php 
                                            if ($user_fetch_data->num_rows() > 0) {
                                                foreach ($user_fetch_data->result() as $row) {
                                                    ?>

                                            <tr>

                                                <td> <button type="submit" value="<?php echo $row->UserName ?>"
                                                        name="checked_id[]" class="btn btn-link ">
                                                    <?php echo $row->Name ?>

                                                    </button></td>
                                                <td>
                                                    <?php echo $row->UserName  ?>
                                                </td>

                                                <td>
                                                    <?php echo $row->Position  ?>
                                                </td>
                                                <td>
                                                    <?php echo $row->ContactNumber  ?>
                                                </td>

                                            </tr>


                                            <?php

                                        }
                                    } else {
                                        ?>
                                            <tr>
                                                <td colspan="3"> No Data Found </td>
                                            </tr>
                                            <?php 
                                        }
                                        ?>

                                        </tbody>
                                    </table>
                                    
                                </form>
                            </div>
    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
