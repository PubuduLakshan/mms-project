<div class="product-tab-list tab-pane fade" id="Delete">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="review-content-section">
                <div class="row">
                    <?php if (!empty($use)) {
                        foreach ($use as $row) { ?>
                    <?php echo form_open('User/deletedata'); ?>

                    <div class="form-group-inner">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label class="login2">User
                                    Name</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <input type="text" class="form-control" value="<?php echo $row['UserName']; ?>"
                                    disabled />
                                <input type="hidden" class="form-control" name="UserName"
                                    value="<?php echo $row['UserName']; ?>" required />
                            </div>
                        </div>
                    </div>

                    <div class="form-group-inner">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label class="login2">Name</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <input type="text" class="form-control" name="Name" value="<?php echo $row['Name']; ?>"
                                    disabled />
                            </div>
                        </div>
                    </div>

                    <div class="form-group-inner">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label class="login2">Category</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <input type="text" class="form-control" placeholder="Enter Contact Number"
                                    name="Position" value="<?php echo $row['Position']; ?>" disabled />
                            </div>
                        </div>
                    </div>

                    <div class="form-group-inner">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <label class="login2">Contact Number</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <input type="text" class="form-control" value="<?php echo $row['ContactNumber']; ?>"
                                    name="ContactNumber" disabled />
                            </div>
                        </div>
                    </div>


                    

                            <div class="login-btn-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <div class="login-horizental">
                                            <input type="submit" name="delete" value="Delete User"
                                                class="btn btn-sm btn-primary login-submit-cs" />
                                                <?php echo form_close(); ?>
                                            <a data-dismiss="modal" href="#"><button
                                                    class="btn btn-sm btn-primary login-submit-cs"
                                                    type="button">Cancel</button></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php 
                }
            } else { ?>
                    <tr>
                        <td colspan="5"> Value Not Selected</td>
                    </tr>
                    <?php 
                } ?>
                        
                    </div>

            </div>
        </div>
    </div>
</div>