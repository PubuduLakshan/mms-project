
<div class="product-tab-list tab-pane fade" id="Edit2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="review-content-section">
                <div class="row">
                    <?php if (!empty($use)) {
                        foreach ($use as $row) { ?>
                    <?php echo form_open('User/updatedata'); ?>

                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <label class="login2">User
                                            Name</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <input type="text" class="form-control" value="<?php echo $row['UserName']; ?>"
                                            disabled />
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <input type="hidden" class="form-control" name="UserName"
                                            value="<?php echo $row['UserName']; ?>" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <label class="login2">Name</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <input type="text" class="form-control" placeholder="Enter Name" name="Name"
                                            value="<?php echo $row['Name']; ?>" required />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <label class="login2">Category</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <select class="form-control" id="sel1" name="Position">
                                            <option>
                                                <?php echo $row['Position']; ?>
                                            </option>
                                            <option>System Admin
                                            </option>
                                            <option>Head Office Admin
                                            </option>
                                            <option>Project Manager
                                            </option>
                                            <option>Site QS</option>
                                            <option>Site SK</option>
                                            <option>Site ASK</option>
                                        </select>
                                    </div>
                                    <!-- <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <input type="text" class="form-control" placeholder="Enter Category" name="Position" value="<?php echo $row['Position']; ?>" required />
                                    </div> -->
                                </div>
                            </div>
                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <label class="login2">Status</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <select class="form-control" id="sel1" name="Status">
                                            <option>
                                                <?php echo $row['Status']; ?>
                                            </option>
                                            <option>ACTIVE
                                            </option>
                                            <option>BLOCK
                                            </option>
                                            
                                        </select>
                                    </div>
                                    <!-- <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <input type="text" class="form-control" placeholder="Enter Category" name="Position" value="<?php echo $row['Position']; ?>" required />
                                    </div> -->
                                </div>
                            </div>

                            <div class="form-group-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <label class="login2">Contact Number</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <input type="text" id="ContactNumber3" class="form-control" placeholder="Enter Contact Number"
                                            name="ContactNumber" value="<?php echo $row['ContactNumber']; ?>"
                                            required />
                                            <div role="alert" id="cont3"></div>
                                    </div>
                                </div>
                            </div>


                            <div class="login-btn-inner">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <div class="login-horizental">
                                            <input type="submit" name="update" value="Update"
                                                class="btn btn-sm btn-primary login-submit-cs" />
                                            <?php echo form_close(); ?>
                                            <a data-dismiss="modal" href="#"><button
                                                    class="btn btn-sm btn-primary login-submit-cs"
                                                    type="button">Cancel</button></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php 
                        }
                    } else { ?>
                            <tr>
                                <td colspan="5"> Value Not Selected</td>
                            </tr>
                            <?php 
                        } ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>


<script>
var Length = 10;
$(document).ready(function(){
    $('#ContactNumber3').on('keydown keyup change', function(){
var mobileNum = $(this).val();
var validateMobNum= /^\d*(?:\.\d{1,2})?$/;
if (validateMobNum.test(mobileNum ) && mobileNum.length == Length) {
    $('#cont3').css('color', 'green');
    $('#cont3').text('Valid phone number');
}
else {
    $('#cont3').css('color', 'red');
    $('#cont3').text('Invalid number type/length ');
}

});
});
</script>
