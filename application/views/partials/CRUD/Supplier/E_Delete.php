<div class="product-tab-list tab-pane fade" id="Delete1">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="review-content-section">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
														<div class="devit-card-custom">
                                                        <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                            <label
                                                                                                class="login2">Supplier
                                                                                                Name</label>
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                            <input type="text"
                                                                                                class="form-control"
                                                                                                placeholder="Enter Supplier Name" name="suppliername" required disabled/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                            <label class="login2">Supplier Code</label>
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                            <input type="text"
                                                                                                class="form-control"
                                                                                                placeholder="Enter Supplier Code" name="suppliercode" required disabled/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                            <label
                                                                                                class="login2">Contact Number</label>
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                            <input type="text"
                                                                                                class="form-control"
                                                                                                placeholder="Enter Contact Number" name="contactnumber" required disabled/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group-inner">
                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                            <label
                                                                                                class="login2">Address</label>
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                            <input type="text"
                                                                                                class="form-control"
                                                                                                placeholder="Enter Address" name="address" required disabled/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="login-btn-inner">

                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                                            <div
                                                                                                class="login-horizental">
                                                                                                <button
                                                                                                    class="btn btn-sm btn-primary login-submit-cs"
                                                                                                    type="submit">Add
                                                                                                    Supplier</button>

                                                                                                <a data-dismiss="modal"
                                                                                                    href="#"><button
                                                                                                        class="btn btn-sm btn-primary login-submit-cs"
                                                                                                        type="button">Cancel</button></a>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                
														</div>
													</div>
												</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>