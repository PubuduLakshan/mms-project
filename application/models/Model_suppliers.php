<?php

class Model_suppliers extends CI_Model
{

    function insertSuppliersData()
    {

        $data = array(

            'SupplierName' => $this->input->post('suppliername', true),
            'SupplierCode' => $this->input->post('suppliercode', true),
            'ContactNo' => $this->input->post('contactno', true),
            'Address' => $this->input->post('address', true)

        );
        return $this->db->insert('suppliermaster', $data);
    }

    function suppliers_fetch_data()
    {

        $query = $this->db->get("suppliermaster");
        return $query;
    }
}
