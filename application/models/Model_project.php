<?php

class Model_project extends CI_Model
{

    function insertProjectData()
    {

        $data = array(

            'ProjectName' => $this->input->post('projectname', true),
            'ProjectCode' => $this->input->post('projectcode', true),
            'MainLocation' => $this->input->post('mainlocation', true),
            'Description' => $this->input->post('description', true)
            
        );
        return $this->db->insert('projectmaster', $data);
    }

    function project_fetch_data()
    {

        $query = $this->db->get("projectmaster");
        return $query;
    }
}
