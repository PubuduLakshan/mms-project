<?php

class User_Delete extends CI_Model
{
    function fetch_data(){
        $this->db->select("*");
        $this->db->form("usermaster");
        $this->db->order_by('id','desc');
        return  $this->db->get();

    }
    function delete($id){
        $this->db->where('id',$id);
        $this->db->delete('usermaster');
    }

    public function delete_data($UserName){
        $this->db->where("UserName",$UserName);
        $this->db->delete("usermaster");
    }
}
?>