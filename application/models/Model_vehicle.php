<?php

class Model_vehicle extends CI_Model
{

    function insertVehicleData()
    {

        $data = array(

            'VehicleNo' => $this->input->post('vehiclenumber', true),
            'Description' => $this->input->post('description', true),
            'VehicleType' => $this->input->post('type', true),
            'Capacity' => $this->input->post('capacity', true),
            'UOM' => $this->input->post('uom', true)
            
        );
        return $this->db->insert('vehiclemaster', $data);
    }

    function vehicle_fetch_data()
    {

        $query = $this->db->get("vehiclemaster");
        return $query;
    }
}
