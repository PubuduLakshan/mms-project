<?php

class Model_permission extends CI_Model
{


    function user_fetch_data()
    {

        $this->db->select('*');
        $this->db->where(array('PermissionGrant' => ''));
        $this->db->from('usermaster');

        // // Execute the query.
        $query1 = $this->db->get();
        return $query1;
    }



    function user_permission_data()
    {

        $this->db->select('*');
        $this->db->where(array('PermissionGrant' => 'YES'));
        $this->db->from('usermaster');

        // // Execute the query.
        $query = $this->db->get();
        return $query;
    }

    function insertPermission($data, $UserName,$Position)
    {

        $this->load->database();
        $this->db->where('UserName', $UserName);
        $this->db->where('Position', $Position);
        $this->db->update('usermaster', $data);
        //$this->db->query("YOUR QUERY");

        return true;
    }

    function auto($uname1)
 {
  $this->db->where('UserName', $uname1);
  $query = $this->db->get('usermaster');
  
  
  foreach($query->result() as $row)
  {
   $output .= '<option value="'.$row->Position.'">'.$row->Position.'</option>';
  }
  return $output;

 }



    function grant_Permission(
        $UserName,
        $ProjectNew,
        $ProjectView,
        $ProjectEdit,
        $ProjectDelete,
        $UserNew,
        $UserView,
        $UserEdit,
        $UserDelete,
        $UserBlock,
        $QRCodeAdd,
        $QRCodeView,
        $QRCodeEdit,
        $QRCodeDelete,
        $QRCodeBlock,
        $QRCodeGeneration,
        $SupplierNew,
        $SupplierView,
        $SupplierEdit,
        $SupplierDelete,
        $SupplierBlock,
        $LocationNew,
        $LocationView,
        $LocationEdit,
        $LocationDelete,
        $SQVDView,
        $SQVDEdit,
        $SQVDPost,
        $SQVDManual,
        $SQView,
        $SQEdit,
        $ReportGen,
        $PermissionSetup,
        $ProjectAssign
    ) {
        $query = $this->db->query("update usermaster set 
            
            ProjectNew=  '$ProjectNew',
            ProjectView=  '$ProjectView',
            ProjectEdit= '$ProjectEdit',
            ProjectDelete=  '$ProjectDelete',
            UserNew=  '$UserNew',
            UserView=  '$UserView',
            UserEdit= '$UserEdit',
            UserDelete='$UserDelete',
            UserBlock='$UserBlock',
            QRCodeAdd=' $QRCodeAdd',
            QRCodeView='$QRCodeView',
            QRCodeEdit='$QRCodeEdit',
            QRCodeDelete='$QRCodeDelete',
            QRCodeBlock='$QRCodeBlock',
            QRCodeGeneration='$QRCodeGeneration',
            SupplierNew='$SupplierNew',
            SupplierView='$SupplierView',
            SupplierEdit='$SupplierEdit',
            SupplierDelete='$SupplierDelete',
            SupplierBlock='$SupplierBlock',
            LocationNew='$LocationNew',
            LocationView='$LocationView',
            LocationEdit='$LocationEdit',
            LocationDelete='$LocationDelete',
            SQVDView='$SQVDView',
            SQVDEdit='$SQVDEdit',
            SQVDPost='$SQVDPost',
            SQVDManual=' $SQVDManual',
            SQView='$SQView',
            SQEdit='$SQEdit',
            ReportGen='$ReportGen',
            PermissionSetup='$PermissionSetup',
            ProjectAssign='$ProjectAssign' where UserName='$UserName' ");
    }
}