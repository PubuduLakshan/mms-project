<?php

class Model_location extends CI_Model
{

    function insertLocationData()
    {

        $data = array(

            'Location' => $this->input->post('locationname', true),
            'LocationCode' => $this->input->post('locationcode', true),
            'Description' => $this->input->post('description', true),
            'ProjectName' => $this->input->post('projectname', true)
        );
        return $this->db->insert('locationsubmaster', $data);
    }


    function location_fetch_data()
    {

        $query = $this->db->get("locationsubmaster");
        return $query;
    }

    public function project_fetch_data()
    {
        $query = $this->db->get("projectmaster");
        return $query;
        }
      

    
}