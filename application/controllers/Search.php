<?php 

class Search extends CI_Controller {

 public function index()
 {
  $this->load->view('check');
 }

 public function checkUsername()
 {
  $this->load->model('Search_model');
  if($this->Search_model->getUsername($_POST['UserName'])){
   echo '<label class="text-danger"><span><i class="fa fa-times" aria-hidden="true">
   </i> This user is already registered</span></label>';
  }
  else {
   echo '<label class="text-success"><span><i class="fa fa-check-circle-o" aria-hidden="true"></i> Username is available</span></label>';
  }
 }

}