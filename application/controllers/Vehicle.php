<?php
class Vehicle extends CI_Controller
{
    public function vehicles()
    {

        if ($this->session->userdata('username') != "") {
            redirect('Vehicle/vehicleView');
        } else {
            $this->load->view('index');
        }
    }

    public function register()
    {


        $this->form_validation->set_rules('vehiclenumber', 'vehiclenumber', 'required|is_unique[vehiclemaster.VehicleNo]');
        $this->form_validation->set_rules('description', 'description', 'required');
        $this->form_validation->set_rules('type', 'type', 'required');
        $this->form_validation->set_rules('capacity', 'capacity', 'required');
        $this->form_validation->set_rules('uom', 'uom', 'required');
        
        if ($this->form_validation->run() == false) {
            redirect('Vehicle/vehicleView');
        } else {
            $this->load->model('Model_vehicle');
            $response = $this->Model_vehicle->insertVehicleData();
            if ($response) {
                $this->session->set_flashdata('msg', 'Inserted Successfully.');
                redirect('Vehicle/vehicleView');
            } else {
                $this->session->set_flashdata('msg', 'Something Went Wrong');
                redirect('Vehicle/vehicleView');
            }
        }
    }

    public function vehicleView()
    {

        if ($this->session->userdata('username') != "") {
            $this->load->model("Model_vehicle");
            $data["vehicle_fetch_data"] = $this->Model_vehicle->vehicle_fetch_data();
            $this->load->view('vehicle',$data); 
        } else {
            $this->load->view('index');
        }
    }
}