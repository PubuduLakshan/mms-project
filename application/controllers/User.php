<?php
class User extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // Load user model
        $this->load->model('Model_user');
    }

    public function index()
    {

        $this->load->view('index');
    }
    public function register()
    {

        // echo "sign up successful";

        $this->form_validation->set_rules('username', 'username', 'required|is_unique[usermaster.UserName]|min_length[5]');
        $this->form_validation->set_rules('name', ' name', 'required');
        $this->form_validation->set_rules('password', 'password', 'required|min_length[5]');
        $this->form_validation->set_rules('catagory', 'catagory', 'required');
        $this->form_validation->set_rules('contact', 'contact', 'required|min_length[10]|max_length[10]');
        if ($this->form_validation->run() == false) {
            redirect('User/users');
        } else {
            $this->load->model('Model_user');
            $response = $this->Model_user->insertUserdata();
            if ($response) {
                $this->session->set_flashdata('msg', 'Inserted Successfully.');
                redirect('User/users');
            } else {
                $this->session->set_flashdata('msg', 'Something Went Wrong');
                redirect('User/users');
            }
        }
    }

    public function Login()
    {

        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('index');
        } else {

            $this->load->model('Model_user');
            $result = $this->Model_user->LoginUser();

            if ($result != false) {
                $user_data = array(
                    'username' => $result->UserName,
                    'position' => $result->Position,
                    'name' => $result->Name,
                    'ProjectNew' => $result->ProjectNew,
                    'ProjectView' => $result->ProjectView,
                    'ProjectEdit' => $result->ProjectEdit,
                    'ProjectDelete' => $result->ProjectDelete,

                    'ProjectNew' => $result->ProjectNew,
                    'ProjectView' => $result->ProjectView,
                    'ProjectEdit' => $result->ProjectEdit,
                    'ProjectDelete' => $result->ProjectDelete,

                    'UserNew' => $result->UserNew,
                    'UserView' => $result->UserView,
                    'UserEdit' => $result->UserEdit,
                    'UserDelete' => $result->UserDelete,

                    'QRCodeNew' => $result->QRCodeNew,
                    'QRCodeView' => $result->QRCodeView,
                    'QRCodeEdit' => $result->QRCodeEdit,
                    'QRCodeDelete' => $result->QRCodeDelete,

                    'VehicleNew' => $result->VehicleNew,
                    'VehicleView' => $result->VehicleView,
                    'VehicleEdit' => $result->VehicleEdit,
                    'VehicleDelete' => $result->VehicleDelete,

                    'SupplierNew' => $result->SupplierNew,
                    'SupplierView' => $result->SupplierView,
                    'SupplierEdit' => $result->SupplierEdit,
                    'SupplierDelete' => $result->SupplierDelete,

                    'LocationNew' => $result->LocationNew,
                    'LocationView' => $result->LocationView,
                    'LocationEdit' => $result->LocationEdit,
                    'LocationDelete' => $result->LocationDelete,
                    'loggedin' => true
                );
                $this->session->set_userdata($user_data);
                $this->session->set_flashdata('welocme', 'Welcome back');
                redirect('User/users');
            } else {
                $this->session->set_flashdata('errmsg', 'Wrong username or password');
                redirect('User/index');
            }
        }
    }



    public function home()
    {

        if ($this->session->userdata('username') != "") {
            $this->load->view('home');
        } else {
            $this->load->view('index');
        }
    }




    public function users()
    {

        if ($this->session->userdata('username') != "") {


            // $this->load->model("Model_user");

            $data1 = array();

            if ($ids = $this->input->post('checked_id')) {
                // print_r($ids);

                if (!empty($ids)) {
                    // $this->load->model("Model_user");
                    $this->Model_user->show($ids);

                    $data['use'] = $this->Model_user->getRows();

                    if ($data1) {


                        $data1['statusMsg'] = 'Selected users have been deleted successfully.';
                    } else {
                        $data1['statusMsg'] = 'Some problem occurred, please try again.';
                    }
                } else {
                    $data1['statusMsg'] = 'Select at least 1 record to delete.';
                }
            }
            $data["user_fetch_data"] = $this->Model_user->user_fetch_data();

            $this->load->view('user', $data);
        } else {
            $this->load->view('index');
        }
    }


    

    public function updatedata()
    {

        $UserName = $this->input->post('UserName');
        $Name = $this->input->post('Name');
        $Position = $this->input->post('Position');
        $ContactNumber = $this->input->post('ContactNumber');
        $Status = $this->input->post('Status');
        $this->Model_user->update_records($UserName, $Name, $Position, $ContactNumber, $Status);
        // echo "Date updated successfully !”;

        redirect('User/users');

        //   print_r($UserName);  
    }

    public function deletedata()
    {

        $UserName = $this->input->post('UserName');

        $this->Model_user->deletedata($UserName);
        // echo "Date updated successfully !”;
        redirect('User/users');

        //   print_r($UserName);  
    }


    public function LogoutUser()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('name');
        $this->session->unset_userdata('loggedin');
        redirect('User/index');
    }

    public function checkUsername()
    {
    $this->load->model('Model_user');
    if($this->Model_user->getUsername($_POST['username1'])){
       
    echo '<span style="color:red"><i class="fa fa-times" aria-hidden="true"></i> 
    This user is already registered</span>';
  }
  else {
   echo '<span style="color:green"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Username is available</span>';
  }
 }



}