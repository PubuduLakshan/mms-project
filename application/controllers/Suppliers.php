<?php
class Suppliers extends CI_Controller
{
    public function supplier()
    {

        if ($this->session->userdata('username') != "") {
            redirect('Suppliers/suppliersView');
        } else {
            $this->load->view('index');
        }
    }

    public function register()
    {


        $this->form_validation->set_rules('suppliername', 'suppliername', 'required');
        $this->form_validation->set_rules('suppliercode', 'suppliercode', 'required|is_unique[suppliermaster.SupplierCode]');
        $this->form_validation->set_rules('contactno', 'contactno', 'required|is_unique[suppliermaster.ContactNo]');
        $this->form_validation->set_rules('address', 'address', 'required');
        
        
        if ($this->form_validation->run() == false) {
            redirect('Suppliers/suppliersView');
        } else {
            $this->load->model('Model_suppliers');
            $response = $this->Model_suppliers->insertSuppliersData();
            if ($response) {
                $this->session->set_flashdata('msg', 'Inserted Successfully.');
                redirect('Suppliers/suppliersView');
            } else {
                $this->session->set_flashdata('msg', 'Something Went Wrong');
                redirect('Suppliers/suppliersView');
            }
        }
    }

    public function suppliersView()
    {

        if ($this->session->userdata('username') != "") {
            $this->load->model("Model_suppliers");
            $data["suppliers_fetch_data"] = $this->Model_suppliers->suppliers_fetch_data();
            $this->load->view('suppliers',$data); 
        } else {
            $this->load->view('index');
        }
    }
}