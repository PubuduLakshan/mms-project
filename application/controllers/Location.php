<?php
class Location extends CI_Controller
{
    public function locations()
    {

        if ($this->session->userdata('username') != "") {
            redirect('Location/locationView');
        } else {
            $this->load->view('index');
        }
    }

    public function add()
    {
        
        $this->form_validation->set_rules('locationname', 'locationname', 'required');
        $this->form_validation->set_rules('locationcode', 'locationcode', 'required|is_unique[locationsubmaster.LocationCode]');
        $this->form_validation->set_rules('description', 'description', 'required');
        $this->form_validation->set_rules('projectname', 'projectname', 'required');
        
        if ($this->form_validation->run() == false) {
            redirect('Location/locationView');
        } else {
            $this->load->model('Model_location');
            $response = $this->Model_location->insertLocationData();
            if ($response) {
                $this->session->set_flashdata('msg', 'Inserted Successfully.');
                redirect('Location/locationView');
            } else {
                $this->session->set_flashdata('msg', 'Something Went Wrong');
                redirect('Location/locationView');
            }
        }

    }

    public function locationView()
    {

        if ($this->session->userdata('username') != "") {
            $this->load->model("Model_location");
            $data["location_fetch_data"] = $this->Model_location->location_fetch_data();
            $data["project_fetch_data"] = $this->Model_location->project_fetch_data();
            $this->load->view('location',$data); 

          
        } else {
            $this->load->view('index');
        }
    }

}

