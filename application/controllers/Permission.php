<?php
class Permission extends CI_Controller
{
    public function index()
    {
        $this->load->view('permission');
    }
    public function permission1()
    {

        if ($this->session->userdata('username') != "") {
            redirect('Permisson/permissionView');
        } else {
            $this->load->view('index');
        }
    }


    public function permissionView()
    {

        if ($this->session->userdata('username') != "") {
            $this->load->model("Model_permission");
            // $UserName= $this->load->post('Uasername');
            $data["user_fetch_data"] = $this->Model_permission->user_fetch_data();
            $data["user_permission_data"] = $this->Model_permission->user_permission_data();
            $this->load->view('permission', $data);
        } else {
            $this->load->view('index');
        }
    }

    // function autofill()
    // {
    //  if($this->input->post('country_id'))
    //  {
    //   echo $this->Model_permission->auto($this->input->post('country_id'));
    //  }
    // }

    public function autofill()
    {
    $this->load->model('Model_permission');
    $result = $this->Model_permission->auto($_POST['uname1']);
       
    echo $result;
        
  
  
 }

    //Permission setup
    public function userAdd()
    {

        $Position = $this->input->post('Position');
        $UserName = $this->input->post('UserName');
        
        $data = array('Position' => $Position, 'UserName' => $UserName, 'PermissionGrant' => "YES");
        //print_r($data);

        $this->load->model('Model_permission');
        $this->Model_permission->insertPermission($data, $UserName,$Position);

        $data["user_permission_data"] = $this->Model_permission->user_permission_data();
        redirect('Permission/permissionView');
    }

    public function grant()
    {


        // $UserName = $_POST['UserName'];
        // $ProjectNew = $_POST['ProjectNew'];
        // $ProjectView = $_POST['ProjectView'];
        // $ProjectEdit = $_POST['ProjectEdit'];
        // $ProjectDelete = $_POST['ProjectDelete'];




        $UserName = $this->input->post('UserName');

        $ProjectNew = $this->input->post('ProjectNew');
        $ProjectView = $this->input->post('ProjectView');
        $ProjectEdit = $this->input->post('ProjectEdit');
        $ProjectDelete = $this->input->post('ProjectDelete');

        $UserNew = $this->input->post('UserNew');
        $UserView = $this->input->post('UserView');
        $UserEdit = $this->input->post('UserEdit');
        $UserDelete = $this->input->post('UserDelete');
        $UserBlock = $this->input->post('UserBlock');

        $QRCodeAdd = $this->input->post('QRCodeAdd');
        $QRCodeView = $this->input->post('QRCodeView');
        $QRCodeEdit = $this->input->post('QRCodeEdit');
        $QRCodeDelete = $this->input->post('QRCodeDelete');
        $QRCodeBlock = $this->input->post('QRCodeBlock');

        $QRCodeGeneration = $this->input->post('QRCodeGeneration');

        $SupplierNew = $this->input->post('SupplierNew');
        $SupplierView = $this->input->post('SupplierView');
        $SupplierEdit = $this->input->post('SupplierEdit');
        $SupplierDelete = $this->input->post('SupplierDelete');
        $SupplierBlock = $this->input->post('SupplierBlock');

        $LocationNew = $this->input->post('LocationNew');
        $LocationView = $this->input->post('LocationView');
        $LocationEdit = $this->input->post('LocationEdit');
        $LocationDelete = $this->input->post('LocationDelete');


        $SQVDView = $this->input->post('SQVDView');
        $SQVDEdit = $this->input->post('SQVDEdit');
        $SQVDPost = $this->input->post('SQVDPost');
        $SQVDManual = $this->input->post('SQVDManual');

        $SQView = $this->input->post('SQView');
        $SQEdit = $this->input->post('SQEdit');

        $ReportGen = $this->input->post('ReportGen');

        $PermissionSetup = $this->input->post('PermissionSetup');

        $ProjectAssign = $this->input->post('ProjectAssign');

        $this->load->model('Model_permission');
        $this->Model_permission->grant_Permission(
            $UserName,
            $ProjectNew,
            $ProjectView,
            $ProjectEdit,
            $ProjectDelete,
            $UserNew,
            $UserView,
            $UserEdit,
            $UserDelete,
            $UserBlock,
            $QRCodeAdd,
            $QRCodeView,
            $QRCodeEdit,
            $QRCodeDelete,
            $QRCodeBlock,
            $QRCodeGeneration,
            $SupplierNew,
            $SupplierView,
            $SupplierEdit,
            $SupplierDelete,
            $SupplierBlock,
            $LocationNew,
            $LocationView,
            $LocationEdit,
            $LocationDelete,
            $SQVDView,
            $SQVDEdit,
            $SQVDPost,
            $SQVDManual,
            $SQView,
            $SQEdit,
            $ReportGen,
            $PermissionSetup,
            $ProjectAssign
        );
        redirect('Permission/permissionView');
    }
}

