<?php
class Project extends CI_Controller
{
    public function projects()
    {

        if ($this->session->userdata('username') != "") {
            redirect('Project/projectView');
        } else {
            $this->load->view('index');
        }
    }


    public function register()
    {

        // echo "sign up successful";

        $this->form_validation->set_rules('projectname', 'projectname', 'required|is_unique[projectmaster.ProjectName]');

        $this->form_validation->set_rules('projectcode', ' projectcode', 'required|is_unique[projectmaster.ProjectCode]');
        $this->form_validation->set_rules('mainlocation', 'mainlocation', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');
        
        if ($this->form_validation->run() == false) {
            redirect('Project/projectView');
            }  else {
            $this->load->model('Model_project');
            $response = $this->Model_project->insertProjectData();
            if ($response) {
                $this->session->set_flashdata('msg', 'Inserted Successfully.');
                redirect('Project/projectView');
            } else {
                $this->session->set_flashdata('msg', 'Something went wrong');
                redirect('Project/projectView');
            }
        }
    }

    public function projectView()
    {

        if ($this->session->userdata('username') != "") {
            $this->load->model("Model_project");
            $data["project_fetch_data"] = $this->Model_project->project_fetch_data();
            $this->load->view('project',$data); 
        } else {
            $this->load->view('index');
        }
    }

}