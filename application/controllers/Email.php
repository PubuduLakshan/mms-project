<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_Controller{
    
    function  __construct(){
        parent::__construct();
    }
    public function email(){
        if ($this->session->userdata('username') != "") {
            $this->load->view('email');
        } else {
            $this->load->view('index');
        }
    }

    
    function send(){
        // Load PHPMailer library
        $this->load->library('phpmailer_lib');
        
        // PHPMailer object
        $mail = $this->phpmailer_lib->load();
        //$message = file_get_contents('mail_templates/sample_mail.html'); 
        // SMTP configuration
        $mail->isSMTP();
        $mail->Host     = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'pubudu.lakshan7@gmail.com';
        $mail->Password = 'Pubuduu7';
        $mail->SMTPSecure = 'ssl';
        $mail->Port     = 465;
        
        $mail->setFrom('pubudu.lakshan7@gmail.com', 'Pubudu');
        //$mail->addReplyTo('pubudu.lakshan7@gmail.com', 'CodexWorld');
        
        // Add a recipient
        
        $to_email = $this->input->post('email'); 
        $mail->addAddress($to_email);
      
        // Email subject
        $subject = $this->input->post('subject'); 
        $mail->Subject =  $subject;
        
        // Set email format to HTML
        $mail->isHTML(true);
        
        //Get mail content from user
        $mailContent = $this->input->post('mailContent');
        

        //Add images to mail body
        $mail->AddEmbeddedImage('D:\NEM.png','NEM');
        $mail->AddEmbeddedImage('D:\Tree.jpg','Tree');

        // Email body content
        $mail->Body = '<h2>NEM Constructions</h2><p><strong>'. $mailContent .'</p></strong> 
                        <br>
                        <footer>
                        <span style="font-size:8.0pt; color:gray">
                        No.629, Baseline Road, Colombo 9, Sri Lanka
                        <br>
                        T: +94 11 267 2071 Ext - 21 | M: +94 77 020 6981 | F: +94 11 268 5132
                        </span>
                        <br>
                        <a href="http://www.nemconstruction.com/">Web-www.nemconstruction.com</a>
                        <br>
                        <img src="cid:NEM">
                        <br>
                        <img src="cid:Tree">
                        <span style="font-size:7.0pt; color:green"> 
                         Think before you print: please consider our environment before printing this e-mail
                         </span>
                        </footer>';

                        

        //$attachment = $this->input->post('uploaded_file');
        //print_r($_FILES['file']);
        for($i=0;$i<count($_FILES['file']['tmp_name']);$i++){
        $mail->AddAttachment($_FILES['file']['tmp_name'][$i],$_FILES['file']['name'][$i]);
        }
       
        $ccmail= $this->input->post('recipient_email');
        //print_r($ccmail);
        //echo $ccmail;
        // Add cc or bcc 
        $mail->addCC($ccmail);



         //Send email
        if(!$mail->send()){
            //echo 'Message could not be sent.';
            //echo 'Mailer Error: ' . $mail->ErrorInfo;
            $this->session->set_flashdata("email_sent","Message could not be sent.");
            redirect('Email/email'); 
        }else{
            //echo $ccmail;
            $this->session->set_flashdata("email_sent","Email sent successfully.");
           redirect('Email/email');
        }
    }
    
}